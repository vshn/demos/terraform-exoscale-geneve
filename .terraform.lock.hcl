# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/exoscale/exoscale" {
  version     = "0.30.0"
  constraints = "~> 0.30.0"
  hashes = [
    "h1:8mVKBkBcbKFkg+eJvDgC7VDoijhoh0vgTeO2SxsvwPI=",
    "zh:187b6e6d30c4af564324654fcf7d585b91a6c8374fddbf45dd7e8aa7a5967497",
    "zh:3ad593f0c76f2613ffb697c2c05cffd3228e01a7a607d25254e62272425b96e0",
    "zh:49d7419dfef6cd937794be6875ad89cf79afec7cdcd6c84a2c9d38147a0425a6",
    "zh:7e0b714091ac3061292d6623d0a00228b6485dca5790de014cf0d97653730127",
    "zh:7f55834169652d8d2a81c8f00eaa6bc16074407d8ebcd5949378bc75c5b161ab",
    "zh:88f3ac75e3d8389036ed4ab1b6ed707ca2ce11b562a6cacab80d190afae9a872",
    "zh:98a13c36d82190c455f6711c5bbd380fc4a7e79c0e8fd164ed9189af7469a301",
    "zh:9c6090d37d0016876600fb36b62fd8a117b5618a1b27c80626a8e56efc6791fd",
    "zh:abb7fb74c4f80da26da32c1674f3bc33d8ace2289c8d289f5028813163b01eb2",
    "zh:d2a3cddadeab8a3e9c6682736477ed38a3da6fbc8bcf7ce6b73998bc145c9894",
    "zh:d51340a013721d11d5886168f6f3eed1d498d42a1512040332828578831614e6",
    "zh:e30fd91a9ccf5a470f2b9fd7a5109083e11361012ad8a892d2644071aa5fa369",
    "zh:f0b10dc49600c9a6e3d7008fbbda9415aaef9f2ca26982ddaf223211b159bb6b",
    "zh:fffb8ccc12d4aef99846b2beef4f255a7d85d2a598a6a96b96690fdb393c53fc",
  ]
}
