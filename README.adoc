= Terraform-GitLab-Exoscale

This project deploys infrastructure in Exoscale using GitLab CI/CD.

It requires the variables `EXOSCALE_API_SECRET` and `EXOSCALE_API_KEY` to be defined in the "Settings -> CI/CD -> Variables" configuration screen.

